## Configuração ELK Cloud  

Este projeto tem por objetivo automatizar a configuração de log_groups do CloudWatch para ingestão de logs no Elastic.co.  
A ideia é baseada na arquitetura que se encontra no projeto.  


## Doc ELK
https://www.elastic.co/guide/en/beats/functionbeat/current/functionbeat-overview.html  
https://www.elastic.co/guide/en/beats/functionbeat/current/change-index-name.html  
https://www.elastic.co/guide/en/beats/functionbeat/current/configuration-template.html  
https://www.elastic.co/guide/en/beats/functionbeat/current/functionbeat-installation-configuration.html  
https://www.elastic.co/pt/blog/using-terraform-with-elastic-cloud  
https://www.elastic.co/guide/en/cloud/current/ec-restful-api.html  
https://www.elastic.co/guide/en/elasticsearch/reference/8.2/query-filter-context.html  


## Commom commands  
./functionbeat remove name_function  
./functionbeat update name_function  
./functionbeat -v -e -d "*" deploy name_function  
./functionbeat -v -e -d "*" deploy elkqitechdev -c functionbeat-dev-qi-tech.yml  
./functionbeat setup -e -c functionbeat-dev-qi-tech.yml  
./functionbeat update -c functionbeat-dev-qi-tech.yml  
